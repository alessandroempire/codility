package com.codility

import org.scalacheck.Prop.forAll
import org.junit.runner.RunWith
import org.scalacheck.Gen
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class DhTest extends FunSuite{

//  test("property1") {
//    val sum = Gen.choose(1,100)
//    val array = Gen.listOfN(100, Gen.choose(1,100)) retryUntil(_.)
//    val property = forAll{
//
//    }
//  }

  test("test1") {
    assert(Dh.solution(Array(1,2,4,4), 8) === true)
  }

  test("test2") {
    assert(Dh.solution(Array(1,2,4,4), 7) === false)
  }

  test("test3") {
    assert(Dh.solution(Array(1), 1) === false)
  }

}
