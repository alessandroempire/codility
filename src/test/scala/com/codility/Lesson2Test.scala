package com.codility

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Lesson2Test extends FunSuite{

  trait testArrays{
    val array1 = Array(3, 8, 9, 7, 6)
    val array2 = Array(1, 2, 3, 4)
  }

  test("rotate array with elements once"){
    new testArrays {
      assert(Lesson2.oddOccurrencesInArray(array1, 1) === Array(6, 3, 8, 9, 7))
    }
  }

  test("rotate array with elements twice"){
    new testArrays {
      assert(Lesson2.oddOccurrencesInArray(array1, 2) === Array(7, 6, 3, 8, 9))
    }
  }

  test("rotate array with elements three times"){
    new testArrays {
      assert(Lesson2.oddOccurrencesInArray(array1, 3) === Array(9, 7, 6, 3, 8))
    }
  }

  test("rotate array with elements four times"){
    new testArrays {
      assert(Lesson2.oddOccurrencesInArray(array2, 4) === array2)
    }
  }

  test("rotate array with elements K >= N times"){
    new testArrays {
      assert(Lesson2.oddOccurrencesInArray(array2, 5) === Array(4, 1, 2, 3))
    }
  }

  test("rotate empty array"){
    val input: Array[Int] = Array()
    val expected: Array[Int] = Array()
    assert(Lesson2.oddOccurrencesInArray(input, 2) === expected)
  }

  test("single element array"){
    val input: Array[Int] = Array(1)
    val expected: Array[Int] = Array(1)
    assert(Lesson2.oddOccurrencesInArray(input, 3) === expected)
  }

  /**
    *
    */
  test("odd elements in an array"){
    val input = Array(9,3,9,3,9,9,7)
    assert(Lesson2.cyclicRotation(input) === 7)
  }

  test("odd elements in an array of a single element"){
    val input = Array(9)
    assert(Lesson2.cyclicRotation(input) === 9)
  }

}
