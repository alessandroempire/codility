package com.codility

import org.scalacheck._
import org.scalacheck.Prop.{forAll, BooleanOperators}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Lesson3FS extends FunSuite {

  test("Jump 1"){
    assert(Lesson3.frogJmp(10, 85, 30) === 3)
  }

  test("Jump Lower Border Case"){
    assert(Lesson3.frogJmp(10, 20, 1) === 10)
  }

  test("Jump Upper Border Case"){
    assert(Lesson3.frogJmp(10, 20, 30) === 1)
  }

  test("Jump No jump Needed"){
    assert(Lesson3.frogJmp(10, 10, 30) === 0)
  }

  test("Jump Many jumps"){
    assert(Lesson3.frogJmp(10, 1000010, 1) === 1000000)
  }

  /**
    * Lesson 2
    */
  test("Example test of array problem"){
    assert(Lesson3.permMissingElem(Array(1,2,3,5)) === 4 )
  }

  test("property of array problem "){
    val propertyOfArray = forAll{ n: Int =>
      (n >= 0 && n <= 100000) ==> {
        val range = 1 to n + 1
        val start = 1
        val end = n + 1
        val rnd = new scala.util.Random
        val missing = start + rnd.nextInt( (end - start) + 1 )
        val array = range.filter(i => i != missing).toArray
        Lesson3.permMissingElem(array) == missing
      }
    }

    propertyOfArray.check()
  }

  /**
    * Lesson 3
    */
  test("Lesson 3 example"){
    assert(Lesson3.tapeEquilibrium(Array(3,1,2,4,3)) === 1)
  }

  test("Lesson 3 two elems"){
    assert(Lesson3.tapeEquilibrium(Array(-1000, 1000)) === 2000)
  }

  test("Lesson 3 another case"){
    assert(Lesson3.tapeEquilibrium(Array(5, 6, 2, 4, 1)) === 4)
  }

  test("Lesson 3 three elemes border case"){
    assert(Lesson3.tapeEquilibrium(Array(5, 2, 3)) === 0)
  }

  test("Tape equilibrium"){
    assert(Lesson3.tapeEquilibrium(Array(0)) === 0)
  }

  test("Tape equilibrium v2"){
    assert(Lesson3.tapeEquilibrium(Array(-1, 14)) === 15)
  }


  test("Lesson 3 property which checks performance but not correcteness. "){
    def tapeDifference(index: Int, array: Array[Int]): Int = {
      val (left, right) = array.splitAt(index)
      Math.abs(left.sum - right.sum)
    }

    val property = forAll(Gen.nonEmptyListOf(Gen.choose(-1000,1000))) { list: List[Int] =>
      val array = list.toArray
      forAll(Gen.oneOf(array.indices)) { index =>
        Lesson3.tapeEquilibrium(array) <= tapeDifference(index, array)
      }
    }

    property.check()
  }

}
