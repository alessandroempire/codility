package com.codility

import scala.collection.immutable.{HashMap, HashSet}

object Dh {

  /**
    * Given an array, find if there are two elements whose sum is equal to k
    * @param a
    * @param k
    * @return
    */
  def solution(a: Array[Int], k: Int): Boolean = {
    a.foldLeft((false, HashSet[Int]())){
      case(acc, e) => if (acc._2.contains(k-e)) (true, acc._2) else (acc._1, acc._2 + e)
    }._1
  }

}
