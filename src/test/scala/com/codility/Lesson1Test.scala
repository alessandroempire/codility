package com.codility

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Lesson1Test extends FunSuite {

  test("solution of 9"){
    assert(Lesson1.binaryGap(9) === 2)
  }

  test("solution of 529"){
    assert(Lesson1.binaryGap(529) === 4)
  }

  test("solution of 20"){
    assert(Lesson1.binaryGap(20) === 1)
  }

  test("solution of 15"){
    assert(Lesson1.binaryGap(15) === 0)
  }

  test("solution of 32"){
    assert(Lesson1.binaryGap(32) === 0)
  }

  test("solution of 1041"){
    assert(Lesson1.binaryGap(1041) === 5)
  }

}
