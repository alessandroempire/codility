package com.codility

import scala.collection.immutable.HashSet

object Lesson2 {

  /**
    * Rotate an array to the right by a given number of steps.
    * A non-empty array A consisting of N integers is given.
    * The array contains an odd number of elements, and each element of the array can be paired with
    * another element that has the same value, except for one element that is left unpaired.
    *
    * @param a
    * @param k
    * @return
    */
  def oddOccurrencesInArray(a: Array[Int], k: Int): Array[Int] = a match {
    case Array() => Array()
    case Array(a) => Array(a)
    case _ => takeAndDropLastK(a, Array(), k % a.length)
  }

  private def takeAndDropLastK(x: Array[Int], y: Array[Int], k: Int): Array[Int] = k match {
    case 0 => y ++ x
    case _ => takeAndDropLastK(x.init, x.last +: y, k-1)
  }


  /**
    * An array A consisting of N integers is given.
    * Rotation of the array means that each element is shifted right by one index,
    * and the last element of the array is moved to the first place.
    * For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7]
    * (elements are shifted right by one index and 6 is moved to the first place).
    *
    * The goal is to rotate array A K times; that is, each element of A will be shifted to the right K times.
    *
    * @param a
    * @return
    */
  def cyclicRotation(a: Array[Int]): Int = {
    a.foldLeft(HashSet[Int]()){
      case (acc, x) =>
        if (acc.contains(x)){
          acc - x
        } else {
          acc + x
        }
    }.head
  }



}
