package com.codility.testUtils

import java.util.concurrent.locks.ReentrantLock

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

private[testUtils] object TestingSparkContext {

  /** lock allows non-Spark tests to still run concurrently */
  private val lock = new ReentrantLock()

  /** global SparkSession that can be re-used between tests */
  private lazy val sparkSession: SparkSession = createLocalSparkSession()

  /** System property can be used to turn off globalSparkContext easily */
  private val useGlobalSparkContext: Boolean = System.getProperty("useGlobalSparkContext", "true").toBoolean

  /**
    * Should be called from before()
    */
  def getSparkSession: SparkSession = {
    if (useGlobalSparkContext) {
      // reuse the global SparkContext
      sparkSession
    }
    else {
      // create a new SparkSession each time
      lock.lock()
      createLocalSparkSession()
    }
  }

  /**
    * Should be called from after()
    */
  def cleanUp(): Unit = {
    if (!useGlobalSparkContext) {
      cleanupSpark()
    }
  }

  private def createLocalSparkSession(serializer: String = "org.apache.spark.serializer.KryoSerializer",
                                      registrator: String = "org.trustedanalytics.atk.graphbuilder" +
                                        ".GraphBuilderKryoRegistrator"): SparkSession = {

    val conf = new SparkConf().set("spark.driver.memory", "4g")

    SparkSession.builder
      .appName("insight")
      .master("local[4]")
      .config("spark.sql.shuffle.partitions", "2")
        .config(conf)
      .getOrCreate()
  }

  /**
    * Shutdown spark and release the lock
    */
  private def cleanupSpark(): Unit = {
    try {
      if (sparkSession != null) {
        sparkSession.stop()
      }
    }
    finally {
      // To avoid Akka rebinding to the same port, since it doesn't unbind immediately on shutdown
      System.clearProperty("spark.driver.port")
      lock.unlock()
    }
  }

}