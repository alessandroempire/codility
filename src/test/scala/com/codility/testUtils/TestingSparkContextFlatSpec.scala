package com.codility.testUtils

import org.apache.spark.sql.{SQLContext, SQLImplicits, SparkSession}
import org.scalatest.{BeforeAndAfter, FlatSpec}

trait TestingSparkContextFlatSpec extends FlatSpec with BeforeAndAfter {

  var sparkSession: SparkSession = TestingSparkContext.getSparkSession

  object testImplicits extends SQLImplicits {
    protected override def _sqlContext: SQLContext = sparkSession.sqlContext
  }

  before {
    sparkSession = TestingSparkContext.getSparkSession
  }

  after {
    TestingSparkContext.cleanUp()
    sparkSession = null
  }

}

