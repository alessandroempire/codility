package com.codility

object Lesson3 {

  /**
    * A small frog wants to get to the other side of the road.
    * The frog is currently located at position X and wants to get to a position greater than or equal to Y.
    * The small frog always jumps a fixed distance, D.
    * Count the minimal number of jumps that the small frog must perform to reach its target
    *
    * @param x
    * @param y
    * @param d
    * @return
    */
  def frogJmp(x: Int, y: Int, d: Int): Int = {
    val yx = y - x
    if (yx % d == 0) yx / d else (yx / d) + 1
  }


  /**
    * An array A consisting of N different integers is given.
    * The array contains integers in the range [1..(N + 1)],
    * which means that exactly one element is missing.
    * Your goal is to find that missing element.
    *
    * @param a
    * @return
    */
  def permMissingElem(a: Array[Int]): Int = {
    val aLength: Long = a.length + 1
    val expected: Long = aLength * (aLength + 1 ) / 2
    val actual: Long = a.sum
    (expected - actual).toInt
  }

  /**
    * A non-empty array A consisting of N integers is given. Array A represents numbers on a tape.
      Any integer P, such that 0 < P < N, splits this tape into two non-empty parts:
        A[0], A[1], ..., A[P − 1] and A[P], A[P + 1], ..., A[N − 1].
      The difference between the two parts is the value of:
        |(A[0] + A[1] + ... + A[P − 1]) − (A[P] + A[P + 1] + ... + A[N − 1])|
      In other words, it is the absolute difference between the sum of the first part and the sum of the second part.
    */
  def tapeEquilibrium(a: Array[Int]): Int = {
    def splitterRec(a: Array[Int], index: Int, sumLeft: Int, sumRight: Int, acc: Int): Int = index match {
      case x if x <= 0 => acc
      case _ => {
        val (left, right) = (sumLeft - a(index), sumRight + a(index))
        val absSum = Math.abs(left - right)
        splitterRec(a, index - 1, left, right, if (absSum <= acc) absSum else acc)
      }
    }

    val lengthOfA = a.length - 1
    val (left, right) = a.splitAt(lengthOfA)
    val (sumLeft, sumRight) = (left.sum, right.sum)
    splitterRec(a, lengthOfA-1, sumLeft, sumRight, Math.abs(sumLeft - sumRight))
  }
}
