package com.codility

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Lesson5Test extends FunSuite{

  /**
    * Passing cars tests
    */
  test("passingCars codility test"){
    assert(Lesson5.passingCars(Array(0,1,0,1,1)) === 5)
  }

  test("passingCars codility border case 1"){
    assert(Lesson5.passingCars(Array(0)) === 0)
  }

  test("passingCars codility border case 2"){
    assert(Lesson5.passingCars(Array(1)) === 0)
  }

  test("passingCars codility border case 3"){
    assert(Lesson5.passingCars(Array(0, 0, 0)) === 0)
  }

  test("passingCars codility border case 4"){
    assert(Lesson5.passingCars(Array(1,1,1,1)) === 0)
  }

  test("passingCars codility normal"){
    assert(Lesson5.passingCars(Array(1,1,0,1,1,1)) === 3)
  }

  test("passingCars codility border case 5"){
    val input = Array(0) ++ (1 to 100000).map(i => if (i%2 == 0) 1 else 0)
    assert(Lesson5.passingCars(input) === -1)
  }

  /**
    *GenomicRangeQuery
    *
    */
  test("GenomicRangeQuery codility example"){
    assert(
      Lesson5.GenomicRangeQuery("CAGCCTA", Array(2,5,0), Array(4,5,6)) ===
      Array(2,4,1)
    )
  }

  /**
    * inAvgTwoSlice
    */
  test("inAvgTwoSlice codility example"){
    assert(
      Lesson5.inAvgTwoSlice(Array(4,2,2,5,1,5,8)) ===
      1
    )
  }

  test("inAvgTwoSlice quadruples"){
    assert(
      Lesson5.inAvgTwoSlice(Array(4,4,2,2)) ===
        2
    )
  }

  test("inAvgTwoSlice identical "){
    assert(
      Lesson5.inAvgTwoSlice(Array(2,2,2,2)) ===
        0
    )
  }

  test("inAvgTwoSlice with just two elements"){
    assert(
      Lesson5.inAvgTwoSlice(Array(4,2)) ===
        0
    )
  }

  test("inAvgTwoSlice with just three elements"){
    assert(
      Lesson5.inAvgTwoSlice(Array(4,2,1)) ===
        1
    )
  }

  /**
    * countDiv
    */
  test("countDiv codility example"){
    assert(
      Lesson5.countDiv(6, 11, 2) === 3
    )
  }

  test("countDiv border case zero"){
    assert(
      Lesson5.countDiv(0, 0, 1) === 1
    )
  }


}
