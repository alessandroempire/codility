package com.codility

import scala.collection.immutable.{HashMap, HashSet}

object Lesson4 {

  /**
    * A non-empty array A consisting of N integers is given.
    * A permutation is a sequence containing each element from 1 to N once, and only once.
    * The goal is to check whether array A is a permutation.
    */
  def permCheck(a: Array[Int]): Int = {
    def maxAndHash(a: Array[Int], index: Int, hash: HashSet[Long], max: Long, acc: Long): Option[(Long, Long)] = index match {
      case -1 => Some((max, acc))
      case _ => {
        val head = a(index)
        if (hash.contains(head)) None
        else maxAndHash(a, index-1, hash + head, if (max < head) head else max, acc + 1)
      }
    }

    maxAndHash(a, a.length-1, HashSet[Long](), Int.MinValue, 0) match {
      case None => 0
      case Some((max, acc)) => if (acc == max) 1 else 0
    }
  }


  /**
    *A small frog wants to get to the other side of a river.
    * The frog is initially located on one bank of the river (position 0) and wants to get to the opposite bank
    * (position X+1). Leaves fall from a tree onto the surface of the river.
    *
    * You are given an array A consisting of N integers representing the falling leaves.
    * A[K] represents the position where one leaf falls at time K, measured in seconds.
    *
    * The goal is to find the earliest time when the frog can jump to the other side of the river.
    * The frog can cross only when leaves appear at every position across the river from 1 to X
    * (that is, we want to find the earliest moment when all the positions from 1 to X are covered by leaves).
    * You may assume that the speed of the current in the river is negligibly small, i.e. the leaves do not change
    * their positions once they fall in the river.
    */
  def frogRiverOne(x: Int, a: Array[Int]): Int = {
    def findK(x: Int, a: Array[Int], index: Int, lengthOfA: Int, acc: Set[Int]): Int =
      if (index == lengthOfA){
        -1
      } else {
        val elem = a(index)
        val newAcc = if (elem <= x) acc + elem else acc
        if (newAcc.size == x) index else findK(x, a, index+1, lengthOfA, newAcc)
      }

    findK(x, a, 0, a.length, Set())
  }

  /**
    * You are given N counters, initially set to 0, and you have two possible operations on them:
    *
    * increase(X) − counter X is increased by 1,
    * max counter − all counters are set to the maximum value of any counter.
    * A non-empty array A of M integers is given. This array represents consecutive operations:
    *
    * if A[K] = X, such that 1 ≤ X ≤ N, then operation K is increase(X),
    * if A[K] = N + 1 then operation K is max counter.
    *
    */

  case class Counters(currentMax: Int, seenMax: Int, hashMap: HashMap[Int, Int])

  def maxCounters(n: Int, a: Array[Int]): Array[Int] = {
    def maxCounterRec(n: Int, a: Array[Int], i: Int, acc: Counters): Counters =
      if (i == a.length)
        acc
      else {
        if (a(i) == n+1){
          val newMax = if (acc.seenMax > acc.currentMax) acc.seenMax else acc.currentMax
          maxCounterRec(n, a, i+1, Counters(newMax, acc.seenMax, new HashMap[Int, Int]()))
        } else {
          //update the map and update the max if necesary
          val x = a(i)
          val y = acc.hashMap.getOrElse(x, acc.currentMax) + 1
          val newMax = if (y > acc.seenMax) y else acc.seenMax
          val newMap = acc.hashMap + (x -> y)
          maxCounterRec(n, a, i+1, Counters(acc.currentMax, newMax, newMap))
        }
      }

    val acc = maxCounterRec(n, a, 0, Counters(0, 0, new HashMap[Int, Int]()))
    val result = new Array[Int](n)
    for (i <- 0 until n){
      result(i) = acc.hashMap.getOrElse(i+1, acc.currentMax)
    }
    result
  }

  /**
    * Given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
    */
  def missingInteger(a: Array[Int]): Int = {
    def createPositiveInts(a: Array[Int], i: Int, positiveInts: HashSet[Int]): HashSet[Int] = {
      if (i >= a.length) positiveInts
      else createPositiveInts(a, i+1,  if (a(i) > 0 ) positiveInts + a(i) else positiveInts)
    }

    def findMin(j: Int, positiveInts: HashSet[Int]): Int =
      if (positiveInts.contains(j)) findMin(j+1, positiveInts) else j

    val positiveInts = createPositiveInts(a, 0, new HashSet[Int]())
    if (positiveInts.isEmpty) 1 else findMin(1, positiveInts)
  }


}

