package com.codility

object Lesson5 {

  /**
    * A non-empty array A consisting of N integers is given.
    * The consecutive elements of array A represent consecutive cars on a road.
    *
    * Array A contains only 0s and/or 1s:
    *
    * 0 represents a car traveling east,
    * 1 represents a car traveling west.
    *
    * The goal is to count passing cars. We say that a pair of cars (P, Q), where 0 ≤ P < Q < N, is passing when P
    * is traveling to the east and Q is traveling to the west.
    *
    * For example, consider array A such that:
    *
    * A[0] = 0
    * A[1] = 1
    * A[2] = 0
    * A[3] = 1
    * A[4] = 1
    * We have five pairs of passing cars: (0, 1), (0, 3), (0, 4), (2, 3), (2, 4).
    */
  def passingCars(a: Array[Int]): Int = {
    def transverseArray(a: Array[Int], i: Int, seenZeros: Int, accSum: Int): Int =
      if (accSum > 1000000000){
        -1
      }
      else if (i >= a.length) {
        accSum
      } else {
        if (a(i) == 0){
          transverseArray(a, i+1, seenZeros + 1, accSum)
        } else {
          transverseArray(a, i+1, seenZeros, accSum + seenZeros)
        }
      }

    transverseArray(a, 0, 0, 0)
  }

  /**
    * A DNA sequence can be represented as a string consisting of the letters A, C, G and T, which correspond
    * to the types of successive nucleotides in the sequence. Each nucleotide has an impact factor,
    * which is an integer. Nucleotides of types A, C, G and T have impact factors of 1, 2, 3 and 4,
    * respectively. You are going to answer several queries of the form: What is the minimal impact factor of
    * nucleotides contained in a particular part of the given DNA sequence?
    */
  def GenomicRangeQuery(s: String, p: Array[Int], q: Array[Int]): Array[Int] = {
    /**
      * First we calculate the prefix sum.
      * Notice that what we calculate is how many times a Genom has appeared until that point.
      */
    val prefixSum: Array[Array[Int]] = Array.ofDim[Int](3, s.length+1)
    def createPrefixSums(i: Int): Unit ={
      if (i < s.length){
        val (a,c,g) = s.charAt(i) match {
          case 'A' => (1,0,0)
          case 'C' => (0,1,0)
          case 'G' => (0,0,1)
          case _ => (0,0,0)
        }
        prefixSum(0)(i+1) = prefixSum(0)(i) + a
        prefixSum(1)(i+1) = prefixSum(1)(i) + c
        prefixSum(2)(i+1) = prefixSum(2)(i) + g
        createPrefixSums(i+1)
      }
    }
    createPrefixSums(0)

    /**
      * Now we go through P and Q
      */
    val result = new Array[Int](p.length)
    def iteratePAndQ(i: Int): Unit = {
      if (i < p.length){
        val from = p(i)
        val to = q(i)+1
        if (prefixSum(0)(to) - prefixSum(0)(from) > 0) {
          result(i) = 1
        } else if (prefixSum(1)(to) - prefixSum(1)(from) > 0){
          result(i) = 2
        } else if (prefixSum(2)(to) - prefixSum(2)(from) > 0) {
          result(i) = 3
        } else {
          result(i) = 4
        }

        iteratePAndQ(i+1)
      }
    }
    iteratePAndQ(0)
    result
  }


  /**
    *
    * A non-empty array A consisting of N integers is given. A pair of integers (P, Q), such that 0 ≤ P < Q < N,
    * is called a slice of array A (notice that the slice contains at least two elements).
    * The average of a slice (P, Q) is the sum of A[P] + A[P + 1] + ... + A[Q] divided by the length of the slice.
    * To be precise, the average equals (A[P] + A[P + 1] + ... + A[Q]) / (Q − P + 1).
    */
  def inAvgTwoSlice(a: Array[Int]): Int = {
    /**
      * The key to solve this is to realize that we only need to check slices of size 2 or 3.
      */
    def recursiveFindMin(index: Int, localMin: Double, indexOfLocalMin: Int): (Int, Double, Int) = {
      if (index < a.length - 2){
        val avgOfTwo: Double = (a(index) + a(index+1)) / 2.0
        val avgOfThree: Double = (a(index) + a(index+1) + a(index+2)) / 3.0
        val min: Double = Math.min(avgOfTwo, avgOfThree)
        if (min < localMin){
          recursiveFindMin(index+1, min, index)
        } else {
          recursiveFindMin(index+1, localMin, indexOfLocalMin)
        }
      } else {
        (index, localMin, indexOfLocalMin)
      }
    }

    val (index, localMin, indexOfLocalMin) = recursiveFindMin(0, Double.MaxValue, 0)
    val avgOfTwo: Double = (a(index) + a(index+1)) / 2.0
    if (avgOfTwo < localMin){
      index
    } else {
      indexOfLocalMin
    }
  }

  /**
    * given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:
    *
    * { i : A ≤ i ≤ B, i mod K = 0 }
    *
    */
  def countDiv(a: Int, b: Int, k: Int): Int = {
    val inclusive = a % k match {
      case 0 => 1
      case _ => 0
    }
    (b / k) - (a / k) + inclusive
  }
}
