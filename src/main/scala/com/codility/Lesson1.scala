package com.codility

object Lesson1 {

  /**
    * A binary gap within a positive integer N is any maximal sequence of consecutive zeros that is
    * surrounded by ones at both ends in the binary representation of N.
    *
    * For example, number 9 has binary representation 1001 and contains a binary gap of length 2.
    * The number 529 has binary representation 1000010001 and contains two binary gaps
    *
    * Find longest sequence of zeros in binary representation of an integer.
    * @param n
    * @return
    */
  def binaryGap(n: Int): Int = {
    val binaryString = n.toBinaryString
    countGaps(binaryString, false, 0, 0)
  }

  private def countGaps(binary: String, wasZero: Boolean, currentGap: Int, biggestGap: Int): Int = {
    if (binary.isEmpty){
      biggestGap
    } else{
      if (binary.head == '0'){
        val counter = if (wasZero) currentGap + 1 else 1
        countGaps(binary.tail, true,counter, biggestGap)
      } else{
        val gap = if (biggestGap >= currentGap) biggestGap else currentGap
        countGaps(binary.tail, false, 0, gap)
      }
    }
  }
}
