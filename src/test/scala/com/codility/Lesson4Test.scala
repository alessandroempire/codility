package com.codility

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalacheck.Gen
import org.scalacheck.Prop.forAll

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class Lesson4Test extends FunSuite{

  /**
    * Perm Check
    */
  test("permCheck1"){
    assert(Lesson4.permCheck(Array(4,1,3,2)) === 1)
  }

  test("permCheck2"){
    assert(Lesson4.permCheck(Array(4,1,3)) === 0)
  }

  test("permCheck3"){
    assert(Lesson4.permCheck(Array()) === 0)
  }

  test("permCheck1 property"){
    val nGen = Gen.choose(1, 10000)
    val property = forAll(nGen){ (n: Int) =>
      val correct = Random.shuffle(1 to n).toArray
      Lesson4.permCheck(correct) === 1
    }

    property.check()
  }

  test("permCheck2 property"){
    val nGen = Gen.choose(1, 10000)
    val property = forAll(nGen){ (n: Int) =>
      val correct = Random.shuffle(1 to n).toArray
      val correct2 = Random.shuffle(1 to n).toArray
      Lesson4.permCheck(correct ++ correct2) === 0
    }

    property.check()
  }

  /**
    * frogRiverOne
    */
  test("codility example"){
    assert(Lesson4.frogRiverOne(5, Array(1,3,1,4,2,3,5,4)) === 6)
  }

  test("codility example extended"){
    assert(Lesson4.frogRiverOne(5, Array(1,5,3,1,4,2,3,5,4)) === 5)
  }

  test("codility example extended 2"){
    assert(Lesson4.frogRiverOne(5, Array(1,3,1,4,2,3,7,4)) === -1)
  }

  test("codility example extended 3"){
    assert(Lesson4.frogRiverOne(2, Array(2)) === -1)
  }

  test("codility example extended 4"){
    assert(Lesson4.frogRiverOne(5, Array(1,3,1,4,2,3,5,4,5)) === 6)
  }

  test("codility example extended 5"){
    assert(Lesson4.frogRiverOne(2, Array(2,2,2,2,2)) === -1)
  }

  test("codility example extended 6"){
    assert(Lesson4.frogRiverOne(3, Array(7, 1, 7, 3)) === -1)
  }

  test("codility example extended 7"){
    assert(Lesson4.frogRiverOne(3, Array(1, 3, 1, 3, 2, 1, 3)) === 4)
  }

  test("codility example extended 8"){
    assert(Lesson4.frogRiverOne(1, Array(8, 5, 1, 7, 7, 6, 8)) === 2)
  }

  test("property of frog example"){
    val positiveGenerator = Gen.choose(1,10) suchThat( i => i > 0 )
    val property = forAll(Gen.nonEmptyListOf(positiveGenerator), positiveGenerator) { (list: List[Int], n: Int) =>
      val array = list.toArray
      val mustHaveValues = (1 to n).toSet

      def aux(): Int = {
        for (j <- 0 until array.length){
          val a = array.slice(0,j+1).filter(y => y <= n).toSet
          if (a== mustHaveValues)
            return j
        }
        -1
      }

      val pos  = aux
      Lesson4.frogRiverOne(n, array) === pos
    }

    property.check()
  }

  /**
    * maxCounters
    */
  test("max counters Codility example"){
    assert(Lesson4.maxCounters(5, Array(3,4,4,6,1,4,4)) === Array(3,2,2,4,2))
  }

  test("max counters border case 1"){
    assert(Lesson4.maxCounters(1, Array(2)) === Array(0))
  }

  test("max counters border case 2"){
    assert(Lesson4.maxCounters(3, Array(4, 4, 4)) === Array(0, 0, 0))
  }

  test("max counters border case 3"){
    assert(Lesson4.maxCounters(1, Array(1)) === Array(1))
  }

  test("max counters border case 4"){
    assert(Lesson4.maxCounters(1, Array(2, 1)) === Array(1))
  }

  test("max counters border case 5"){
    assert(Lesson4.maxCounters(1, Array(1, 2)) === Array(1))
  }

  test("max counters border case 6"){
    assert(Lesson4.maxCounters(5, Array(3, 6, 2, 1, 6, 5, 4, 6)) === Array(3, 3, 3, 3, 3))
  }

  /**
    * missingInteger
    */
  test("missingInteger codility test 1"){
    assert(Lesson4.missingInteger(Array(1, 3, 6, 4, 1, 2)) === 5)
  }

  test("missingInteger codility test 2"){
    assert(Lesson4.missingInteger(Array(1, 2, 3)) === 4)
  }

  test("missingInteger codility test 3"){
    assert(Lesson4.missingInteger(Array(-1, -3)) === 1)
  }

  test("missingInteger codility my test 1"){
    assert(Lesson4.missingInteger(Array(1)) === 2)
  }

  test("missingInteger codility my test 2"){
    assert(Lesson4.missingInteger(Array(-1)) === 1)
  }

  test("missingInteger codility my test 3"){
    assert(Lesson4.missingInteger(Array(2)) === 1)
  }

}
